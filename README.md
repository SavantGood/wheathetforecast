# Weather Forecast
Weather forecasting is the application of science and technology to predict the conditions of the atmosphere for a given location and time.

## Architecture
<img src="image/5.PNG" style="max-width:100%;"/>

## Tech-stack

* Tech-stack
    * Picasso
    * Room
    * Retrofit
    * Toothpick
    * RxJava
* Architecture
    * MVVM
* Tests
    * Unit Tests
    * Mockk

## Screenshots
<img src="image/1.png" width="275" style="max-width:100%;"/> <img src="image/2.png" width="275" style="max-width:100%;"/> <img src="image/3.png" width="275" style="max-width:100%;"/> <img src="image/4.png" width="275" style="max-width:100%;"/>