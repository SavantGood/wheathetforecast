package com.example.weatherforecast.ui.addingcity;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.example.weatherforecast.R;
import com.example.weatherforecast.utils.I18n;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class AddCityActivityTest {

    @Rule
    public ActivityTestRule<AddCityActivity> rule = new ActivityTestRule<>(AddCityActivity.class);


    private AddCityActivity activity;
    private RecyclerView recyclerViewById;

    @Before
    public void setUp() {
        activity = rule.getActivity();
        recyclerViewById = activity.findViewById(R.id.cityNamesRecyclerView);
    }

    @Test
    public void createRecyclerViewAndAdapter() {
        Assert.assertNotNull(recyclerViewById);
        Assert.assertTrue(recyclerViewById.getAdapter() instanceof WeatherDataItemAdapter);
    }

    @Test
    public void whenWriteCorrectCityThenGetResult() throws InterruptedException {
        EditText testEditText = activity.findViewById(R.id.etCityName);
        activity.runOnUiThread(
                () -> testEditText.setText(I18n.NAME_CITY_PERM)
        );
        activity.afterTextChanged(testEditText.getText());
        WeatherDataItemAdapter adapter = (WeatherDataItemAdapter) recyclerViewById.getAdapter();
        Thread.sleep(2000);

        Assert.assertNotNull(adapter);
        Assert.assertEquals(I18n.NAME_CITY_PERM, adapter.getItem(0).getName());
    }

    @Test
    public void whenIncorrectCityThenGetErrorMessage() throws InterruptedException {
        String UNKNOWN_CITY = "unknownCity";
        EditText testEditText = activity.findViewById(R.id.etCityName);
        TextView textView = activity.findViewById(R.id.tErrorMessage);
        activity.runOnUiThread(
                () -> testEditText.setText(UNKNOWN_CITY)
        );
        activity.afterTextChanged(testEditText.getText());
        Thread.sleep(2000);

        Assert.assertEquals(textView.getVisibility(), View.VISIBLE);
        Assert.assertNotEquals("", textView.getText().toString());
    }

    @Test
    public void whenClickOnResultThenCreateNewActivity() throws InterruptedException {
        EditText testEditText = activity.findViewById(R.id.etCityName);
        activity.runOnUiThread(
                () -> testEditText.setText(I18n.NAME_CITY_PERM)
        );
        activity.afterTextChanged(testEditText.getText());
        Thread.sleep(2000);

        ViewHolder viewHolderForLayoutPosition = recyclerViewById.findViewHolderForLayoutPosition(0);
        Assert.assertNotNull(viewHolderForLayoutPosition);
        View itemView = viewHolderForLayoutPosition.itemView.findViewById(R.id.nameCity);
        activity.runOnUiThread(itemView::performClick);
    }
}