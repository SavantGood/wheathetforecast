package com.example.weatherforecast.repositories;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.weatherforecast.data.CitiesDatabase;
import com.example.weatherforecast.data.dao.CityDao;
import com.example.weatherforecast.data.entity.City;
import com.example.weatherforecast.testdatafactories.CityTestDataFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;

@RunWith(AndroidJUnit4.class)
public class CityRepositoryTest {

    private CityDao cityDao;
    private CitiesDatabase citiesDb;

    @Before
    public void setUp() {
        citiesDb = Room.inMemoryDatabaseBuilder(
                ApplicationProvider.getApplicationContext(),
                CitiesDatabase.class
        )
                .build();

        cityDao = citiesDb.getCityDao();
    }

    @Test
    public void testGetAllCities() {
        String TEST_NAME = "testName";
        City testCity = CityTestDataFactory.getValid();
        testCity.setName(TEST_NAME);
        cityDao.insertCity(testCity);
        Flowable<List<City>> allTestCities = cityDao.getAllCities();
        TestSubscriber<List<City>> testSubscriber = allTestCities
                .test()
                .awaitCount(1);

        testSubscriber
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(value -> value.get(0).getName().equals(TEST_NAME));

        testSubscriber.dispose();
    }

    @After
    public void closeDb() {
        citiesDb.close();
    }
}
