package com.example.weatherforecast.repositories;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.weatherforecast.data.CitiesDatabase;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class CitiesDatabaseTest {

    @Test
    public void getInstance() {
        Assert.assertEquals(
                "CitiesDatabase_Impl",
                CitiesDatabase.getInstance(ApplicationProvider.getApplicationContext()).getClass().getSimpleName()
                );
    }
}