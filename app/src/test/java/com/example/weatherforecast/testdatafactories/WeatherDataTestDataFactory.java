package com.example.weatherforecast.testdatafactories;

import com.example.weatherforecast.data.entity.WeatherData;

import java.util.ArrayList;

public class WeatherDataTestDataFactory {
    public static WeatherData getValid() {
        return new WeatherData(
                null,
                new ArrayList<>(),
                "",
                null,
                0,
                null,
                null,
                0,
                null,
                0,
                0,
                "",
                0
        );
    }
}
