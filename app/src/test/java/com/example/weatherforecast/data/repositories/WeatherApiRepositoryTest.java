package com.example.weatherforecast.data.repositories;

import com.example.weatherforecast.BuildConfig;
import com.example.weatherforecast.data.api.ApiService;
import com.example.weatherforecast.data.entity.WeatherData;
import com.example.weatherforecast.testdatafactories.WeatherDataTestDataFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WeatherApiRepositoryTest {

    @InjectMocks
    WeatherApiRepository weatherApiRepository;

    @Mock
    ApiService apiService;

    @Before
    public void setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
    }

    @Test
    public void testGetWeatherByCityName() {
        String TEST_NAME = "testName";
        WeatherData validWeatherData = WeatherDataTestDataFactory.getValid();
        validWeatherData.setName(TEST_NAME);
        when(apiService.getWeatherByCityName(any(), any()))
                .thenReturn(Single.just(validWeatherData));

        TestObserver<WeatherData> testObserver
                = weatherApiRepository.getWeatherByCityName("", BuildConfig.API_KEY)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(value -> value.getName().equals(TEST_NAME));

        testObserver.dispose();
    }
}
