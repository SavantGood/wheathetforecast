package com.example.weatherforecast.di;

import com.example.weatherforecast.data.repositories.CityRepository;

import toothpick.config.Module;

public class CityRepositoryModule extends Module {
    public CityRepositoryModule() {
        bind(CityRepository.class)
                .singleton();
    }
}
