package com.example.weatherforecast.di;

import com.example.weatherforecast.data.repositories.WeatherApiRepository;

import toothpick.config.Module;

public class WeatherApiRepositoryModule extends Module {
    public WeatherApiRepositoryModule() {
        bind(WeatherApiRepository.class)
                .singleton();
    }
}
