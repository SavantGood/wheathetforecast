package com.example.weatherforecast.utils;

public class DegreesFormatter {
    public static String format(float degrees) {
        return Math.round(degrees) + I18n.DEGREES;
    }
}
