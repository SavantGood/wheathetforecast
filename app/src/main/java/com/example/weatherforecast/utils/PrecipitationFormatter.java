package com.example.weatherforecast.utils;

public class PrecipitationFormatter {
    public static String format(int precipitation) {
        return precipitation + " mm";
    }
}
