package com.example.weatherforecast.utils;

public class PercentFormatter {
    public static String format(int humidity) {
        return humidity + " %";
    }
}
