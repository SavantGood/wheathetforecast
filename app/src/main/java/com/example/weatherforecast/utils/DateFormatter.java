package com.example.weatherforecast.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {

    private Date convertFromUnixInDate(int dt, int timezone) {
        return new Date(((dt + timezone) * 1000L));
    }

    public static String formatWithDate(int dt, int timezone) {
        return new SimpleDateFormat(I18n.FORMAT_TIME_AND_DATE, Locale.US)
                .format(new DateFormatter().convertFromUnixInDate(dt, timezone));
    }

    public static String formatOnlyTime(int dt, int timezone) {
        return new SimpleDateFormat(I18n.FORMAT_TIME, Locale.US)
                .format(new DateFormatter().convertFromUnixInDate(dt, timezone));
    }
}
