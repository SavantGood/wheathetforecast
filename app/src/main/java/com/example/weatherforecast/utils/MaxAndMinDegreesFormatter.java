package com.example.weatherforecast.utils;

public class MaxAndMinDegreesFormatter {
    public static String format(float max, float min) {
        return "max " + Math.round(max) + I18n.DEGREES + " / min " + Math.round(min) + I18n.DEGREES;
    }
}
