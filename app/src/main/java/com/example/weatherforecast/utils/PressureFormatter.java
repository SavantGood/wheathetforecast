package com.example.weatherforecast.utils;

public class PressureFormatter {
    public static String format(int pressure) {
        return pressure + " hPa";
    }
}
