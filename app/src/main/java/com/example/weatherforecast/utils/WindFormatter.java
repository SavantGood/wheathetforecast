package com.example.weatherforecast.utils;

public class WindFormatter {
    public static String format(float wind) {
        return wind + " m/s";
    }
}
