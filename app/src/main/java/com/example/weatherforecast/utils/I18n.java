package com.example.weatherforecast.utils;

public class I18n {
    public static final String NAME_DATABASE = "cities";
    public static final String NAME_CITY_PERM = "Perm";
    public static final String KEY_FOR_INTENT = "cityName";

    public static final String FORMAT_TIME_AND_DATE = "MMMM h:mm a";
    public static final String FORMAT_TIME = "h:mm a";
    public static final String DEGREES = "°";

    public static final String NO_NETWORK = "Нет соединения";

}
