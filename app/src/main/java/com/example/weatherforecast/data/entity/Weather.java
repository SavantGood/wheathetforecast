package com.example.weatherforecast.data.entity;

import com.example.weatherforecast.BuildConfig;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Weather {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("main")
    @Expose
    private String main;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("icon")
    @Expose
    private String icon;

    public String getDescription() {
        if (icon == null || icon.isEmpty()) {
            return "";
        }
        return description.substring(0, 1).toUpperCase() + description.substring(1);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getIcon() {
        if (icon == null || icon.isEmpty()) {
            return "";
        }
        return String.format(BuildConfig.IMAGE_URL, icon);
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
