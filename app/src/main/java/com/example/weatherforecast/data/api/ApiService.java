package com.example.weatherforecast.data.api;

import com.example.weatherforecast.data.entity.WeatherData;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET("weather?units=metric")
    Single<WeatherData> getWeatherByCityName(
            @Query("q") String cityName,
            @Query("appid") String apiKey
    );
}
