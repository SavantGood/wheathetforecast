package com.example.weatherforecast.data.repositories;

import com.example.weatherforecast.App;
import com.example.weatherforecast.data.CitiesDatabase;
import com.example.weatherforecast.data.dao.CityDao;
import com.example.weatherforecast.data.entity.City;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class CityRepository {
    private CityDao cityDao;

    @Inject
    public CityRepository() {
        CitiesDatabase database = CitiesDatabase.getInstance(App.instance());
        this.cityDao = database.getCityDao();
    }

    public Flowable<List<City>> getAllCities() {
        return cityDao.getAllCities()
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void deleteCity(City city) {
        cityDao.deleteCity(city);
    }

    public void insertCity(City city) {
        cityDao.insertCity(city);
    }

    public void updateFavoriteState(City city) {
        cityDao.updateFavoriteState(city);
    }

    public List<City> getCityByName(String name) {
        return cityDao.getCityByName(name);
    }
}
