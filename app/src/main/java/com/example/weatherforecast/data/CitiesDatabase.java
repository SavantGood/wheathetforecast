package com.example.weatherforecast.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.weatherforecast.data.dao.CityDao;
import com.example.weatherforecast.data.entity.City;
import com.example.weatherforecast.utils.I18n;

@Database(entities = {City.class}, version = 3, exportSchema = false)
public abstract class CitiesDatabase extends RoomDatabase {
    public abstract CityDao getCityDao();

    private static CitiesDatabase INSTANCE;

    public static CitiesDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, CitiesDatabase.class, I18n.NAME_DATABASE)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();

            if(INSTANCE.getCityDao().getListAllCities().isEmpty()) {
                INSTANCE.getCityDao().insertCity(new City(I18n.NAME_CITY_PERM, true));
            }
        }
        return INSTANCE;
    }
}
