package com.example.weatherforecast.data.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.weatherforecast.data.entity.City;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface CityDao {
    @Query("SELECT * from cities")
    Flowable<List<City>> getAllCities();

    @Query("SELECT * FROM cities WHERE name = :name")
    List<City> getCityByName(String name);

    @Query("SELECT * FROM cities")
    List<City> getListAllCities();

    @Update
    void updateFavoriteState(City city);

    @Delete
    void deleteCity(City city);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertCity(City city);
}
