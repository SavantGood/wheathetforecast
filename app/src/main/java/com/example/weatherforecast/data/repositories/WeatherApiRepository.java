package com.example.weatherforecast.data.repositories;

import com.example.weatherforecast.BuildConfig;
import com.example.weatherforecast.data.api.ApiService;
import com.example.weatherforecast.data.entity.WeatherData;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherApiRepository {
    private ApiService apiService;

    @Inject
    public WeatherApiRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiService = retrofit.create(ApiService.class);
    }

    public Single<WeatherData> getWeatherByCityName(String cityName, String apiKey) {
        return apiService.getWeatherByCityName(cityName, apiKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
