package com.example.weatherforecast;

import android.app.Application;
import android.content.Context;

import com.example.weatherforecast.di.CityRepositoryModule;
import com.example.weatherforecast.di.WeatherApiRepositoryModule;

import toothpick.Scope;
import toothpick.Toothpick;

public class App extends Application {
    private static App instance;
    private Scope appScope;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        instance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appScope = Toothpick.openScope(this);
        appScope.installModules(
                new CityRepositoryModule(),
                new WeatherApiRepositoryModule()
        );
    }

    public static App instance() {
        return instance;
    }

    public static Scope scope() {
        return instance.appScope;
    }
}
