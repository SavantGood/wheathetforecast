package com.example.weatherforecast.ui.citymanagment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.R;
import com.example.weatherforecast.data.entity.City;
import com.example.weatherforecast.ui.base.BaseAdapter;
import com.example.weatherforecast.ui.customclicklistener.OnItemClickListener;

import java.util.List;

import static com.example.weatherforecast.ui.citymanagment.CitiesItemAdapterForEditCities.*;

public class CitiesItemAdapterForEditCities extends BaseAdapter<CitiesItemViewHolderForEditCities, City> {

    public CitiesItemAdapterForEditCities(List<City> items) {
        super(items);
    }

    @Override
    public CitiesItemViewHolderForEditCities onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CitiesItemViewHolderForEditCities(
                LayoutInflater.from(parent.getContext())
                        .inflate(
                                R.layout.item_cities_with_delete_button,
                                parent,
                                false),
                getOnItemClickListener()
        );
    }

    @Override
    public void onBindViewHolder(CitiesItemViewHolderForEditCities holder, int position) {
        holder.bind(getItem(position));
    }

    static class CitiesItemViewHolderForEditCities extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView nameCity;
        private ImageView deleteButton;
        private ImageView favoriteButton;
        private OnItemClickListener listener;

        public CitiesItemViewHolderForEditCities(View itemView, OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;

            nameCity = itemView.findViewById(R.id.nameCity);
            deleteButton = itemView.findViewById(R.id.deleteBtn);
            favoriteButton = itemView.findViewById(R.id.favoriteBtn);

            deleteButton.setOnClickListener(this);
            favoriteButton.setOnClickListener(this);
            nameCity.setOnClickListener(this);
        }

        public void bind(City item) {
            nameCity.setText(item.getName());

            if (item.getFavorite() != null) {
                if (item.getFavorite()) {
                    favoriteButton.setColorFilter(ContextCompat.getColor(itemView.getContext(),
                            R.color.colorWarning));
                } else {
                    favoriteButton.setColorFilter(ContextCompat.getColor(itemView.getContext(),
                            R.color.material_on_background_disabled));
                }
            }
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClickListener(position, v);
            }
        }
    }
}
