package com.example.weatherforecast.ui.viewmodel;

import com.example.weatherforecast.data.entity.City;
import com.example.weatherforecast.data.repositories.CityRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class CityViewModel {

    private CityRepository cityRepository;

    @Inject
    public CityViewModel(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public Flowable<List<City>> getAllCities() {
        return cityRepository.getAllCities();
    }

    public void deleteCity(City city) {
        cityRepository.deleteCity(city);
    }

    public void insertCity(City city) {
        cityRepository.insertCity(city);
    }

    public void updateFavoriteState(City city) {
        cityRepository.updateFavoriteState(city);
    }

    public List<City> getCityByName(String name) {
        return cityRepository.getCityByName(name);
    }

}
