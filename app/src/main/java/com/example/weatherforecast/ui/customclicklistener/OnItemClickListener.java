package com.example.weatherforecast.ui.customclicklistener;

import android.view.View;

public interface OnItemClickListener {
    void onItemClickListener(int position, View view);
}
