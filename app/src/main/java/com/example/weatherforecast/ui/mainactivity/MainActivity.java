package com.example.weatherforecast.ui.mainactivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.App;
import com.example.weatherforecast.BuildConfig;
import com.example.weatherforecast.R;
import com.example.weatherforecast.data.entity.City;
import com.example.weatherforecast.data.entity.Weather;
import com.example.weatherforecast.data.entity.WeatherData;
import com.example.weatherforecast.ui.addingcity.AddCityActivity;
import com.example.weatherforecast.ui.citymanagment.EditCitiesActivity;
import com.example.weatherforecast.ui.customclicklistener.OnItemClickListener;
import com.example.weatherforecast.ui.viewmodel.CityViewModel;
import com.example.weatherforecast.ui.viewmodel.WeatherDataViewModel;
import com.example.weatherforecast.utils.DateFormatter;
import com.example.weatherforecast.utils.DegreesFormatter;
import com.example.weatherforecast.utils.I18n;
import com.example.weatherforecast.utils.MaxAndMinDegreesFormatter;
import com.example.weatherforecast.utils.NameAndSysFormatter;
import com.example.weatherforecast.utils.NetworkManager;
import com.example.weatherforecast.utils.PercentFormatter;
import com.example.weatherforecast.utils.PressureFormatter;
import com.example.weatherforecast.utils.VisibilityFormatter;
import com.example.weatherforecast.utils.WindFormatter;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textview.MaterialTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import toothpick.Toothpick;

public class MainActivity
        extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener,
        OnItemClickListener {
    @Inject
    WeatherDataViewModel weatherDataViewModel;
    @Inject
    CityViewModel cityViewModel;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private List<City> items = new ArrayList<>();
    private List<City> favoriteItems = new ArrayList<>();

    private DrawerLayout drawerLayout;
    private CitiesItemAdapter adapter;
    private CitiesItemAdapter adapterWithFavoriteItems;

    private TextView tCityName;
    private TextView tDate;
    private TextView tDegrees;
    private TextView tWind;
    private TextView tDescription;
    private TextView tFeelsLike;
    private TextView tDegreesMinAndMax;
    private TextView tVisibility;
    private TextView tHumidity;
    private TextView tPressure;
    private TextView tClouds;
    private TextView tSunset;
    private TextView tSunrise;

    private ImageView imgWeather;
    private ImageView imgLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toothpick.inject(this, App.scope());

        Button detailButton = findViewById(R.id.detailButton);
        tCityName = findViewById(R.id.tCityName);
        tDate = findViewById(R.id.tDate);
        tDegrees = findViewById(R.id.tDegrees);
        tWind = findViewById(R.id.tWind);
        imgWeather = findViewById(R.id.imgWeather);
        imgLocation = findViewById(R.id.imgLocation);
        tDescription = findViewById(R.id.tDescription);
        tFeelsLike = findViewById(R.id.tDegreesFeelsLike);
        tDegreesMinAndMax = findViewById(R.id.tDegreesMaxAndMin);
        tVisibility = findViewById(R.id.tVisibility);
        tHumidity = findViewById(R.id.tHumidity);
        tPressure = findViewById(R.id.tPressure);
        tClouds = findViewById(R.id.tClouds);
        tSunrise = findViewById(R.id.tSunrise);
        tSunset = findViewById(R.id.tSunset);
        RecyclerView recyclerView = findViewById(R.id.recyclerViewActivityMain);
        RecyclerView recyclerViewWithFavoriteItems = findViewById(R.id.recyclerViewActivityMainWithFavoriteItems);
        drawerLayout = findViewById(R.id.drawer_layout);
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        NavigationView navigationView = findViewById(R.id.nav_view);

        init();
        setupCitiesRecyclerView(recyclerView);
        setupFavoriteCitiesRecyclerView(recyclerViewWithFavoriteItems);
        setupActionBar(detailButton, toolbar, navigationView);
    }

    private void setupActionBar(Button detailButton, Toolbar toolbar, NavigationView navigationView) {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.openNavDrawer,
                R.string.closeNavDrawer
        );
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        detailButton.setOnClickListener(this);
    }

    private void setupFavoriteCitiesRecyclerView(RecyclerView recyclerViewWithFavoriteItems) {
        adapterWithFavoriteItems = new CitiesItemAdapter(favoriteItems);
        adapterWithFavoriteItems.setOnItemClickListener(this);
        LinearLayoutManager linearLayoutManagerWithFavoriteItems = new LinearLayoutManager(this);
        recyclerViewWithFavoriteItems.setLayoutManager(linearLayoutManagerWithFavoriteItems);
        recyclerViewWithFavoriteItems.setAdapter(adapterWithFavoriteItems);
    }

    private void setupCitiesRecyclerView(RecyclerView recyclerView) {
        adapter = new CitiesItemAdapter(items);
        adapter.setOnItemClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void init() {
        compositeDisposable.add(
                cityViewModel
                        .getAllCities()
                        .subscribe(this::onSuccessGetSortFavoriteList, this::onError)
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nav_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.addButtonFromMainActivity) {
            Intent intent = new Intent(this, AddCityActivity.class);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, EditCitiesActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemClickListener(int position, View view) {
        drawerLayout.closeDrawer(GravityCompat.START);
        compositeDisposable.add(
                weatherDataViewModel.getWeatherByCityName(((MaterialTextView) view).getText().toString(), BuildConfig.API_KEY)
                        .subscribe(this::onSuccess, this::onError)
        );
    }

    private void onSuccess(WeatherData weatherData) {
        Weather weather = weatherData.getWeather().get(0);

        Picasso.get().load(weather.getIcon()).into(imgWeather);
        imgLocation.setImageResource(R.drawable.ic_baseline_location_on_24);
        tCityName.setText(NameAndSysFormatter.format(weatherData.getName(), weatherData.getSys().getCountry()));
        tDate.setText(DateFormatter.formatWithDate(weatherData.getDt(), weatherData.getTimezone()));
        tDegrees.setText(DegreesFormatter.format(weatherData.getMain().getTemp()));
        tWind.setText(WindFormatter.format(weatherData.getWind().getSpeed()));
        tDescription.setText(weather.getDescription());
        tHumidity.setText(PercentFormatter.format(weatherData.getMain().getHumidity()));
        tVisibility.setText(VisibilityFormatter.format(weatherData.getVisibility()));
        tDegreesMinAndMax.setText(MaxAndMinDegreesFormatter.format(weatherData.getMain().getTempMax(), weatherData.getMain().getTempMin()));
        tFeelsLike.setText(DegreesFormatter.format(weatherData.getMain().getFeelsLike()));
        tPressure.setText(PressureFormatter.format(weatherData.getMain().getPressure()));
        tClouds.setText(PercentFormatter.format(weatherData.getClouds().getAll()));
        tSunset.setText(DateFormatter.formatOnlyTime(weatherData.getSys().getSunset(), weatherData.getTimezone()));
        tSunrise.setText(DateFormatter.formatOnlyTime(weatherData.getSys().getSunrise(), weatherData.getTimezone()));
    }

    private void onSuccessGetSortFavoriteList(List<City> cities) {
        String cityName = null;

        if (getIntent().getExtras() != null) {
            cityName = getIntent().getExtras().getString(I18n.KEY_FOR_INTENT);
        }

        for (City item : cities) {
            if (item.getFavorite() != null) {
                if (item.getFavorite()) {
                    favoriteItems.add(item);
                } else {
                    items.add(item);
                }
            }
        }
        adapterWithFavoriteItems.setNewValues(favoriteItems);
        adapter.setNewValues(items);
        adapter.notifyDataSetChanged();
        adapterWithFavoriteItems.notifyDataSetChanged();
        compositeDisposable.add(
                weatherDataViewModel.getWeatherByCityName(
                        cityName == null ? favoriteItems.get(0).getName() : cityName,
                        BuildConfig.API_KEY
                )
                        .subscribe(this::onSuccess, this::onError)
        );
    }

    private void onError(Throwable throwable) {
        NetworkManager.isNetworkAvailable(this);
        throwable.printStackTrace();
    }
}