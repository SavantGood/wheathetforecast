package com.example.weatherforecast.ui.citymanagment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.App;
import com.example.weatherforecast.R;
import com.example.weatherforecast.data.entity.City;
import com.example.weatherforecast.ui.addingcity.AddCityActivity;
import com.example.weatherforecast.ui.mainactivity.MainActivity;
import com.example.weatherforecast.ui.customclicklistener.OnItemClickListener;
import com.example.weatherforecast.ui.viewmodel.CityViewModel;
import com.example.weatherforecast.utils.I18n;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import toothpick.Toothpick;

public class EditCitiesActivity extends AppCompatActivity implements OnItemClickListener {
    @Inject
    CityViewModel cityViewModel;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<City> cityList = new ArrayList<>();
    private CitiesItemAdapterForEditCities adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_cities);
        Toothpick.inject(this, App.scope());

        RecyclerView recyclerView = findViewById(R.id.recyclerViewEditCitiesActivity);
        ImageView backButton = findViewById(R.id.backBtn);
        ImageView startAddActivity = findViewById(R.id.startAddActivity);

        init();
        setupCitiesRecyclerView(recyclerView);

        backButton.setOnClickListener(this::onClickBackButton);
        startAddActivity.setOnClickListener(this::onClickAddButton);
    }

    private void init() {
        compositeDisposable.add(
                cityViewModel.getAllCities().subscribe(this::onSuccess, this::onError)
        );
    }

    private void setupCitiesRecyclerView(RecyclerView recyclerView) {
        adapter = new CitiesItemAdapterForEditCities(cityList);
        adapter.setOnItemClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    private void onSuccess(List<City> cities) {
        cityList = cities;
        adapter.setNewValues(cities);
    }

    private void onError(Throwable throwable) {
        throwable.printStackTrace();
    }

    public void onClickBackButton(View v) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void onClickAddButton(View v) {
        startActivity(new Intent(this, AddCityActivity.class));
    }

    @Override
    public void onItemClickListener(int position, View view) {
        switch (view.getId()) {
            case (R.id.deleteBtn):
                cityViewModel.deleteCity(cityList.get(position));
                break;
            case (R.id.favoriteBtn):
                City favoriteCity = cityViewModel.getCityByName(cityList.get(position).getName()).get(0);
                if (favoriteCity.getFavorite()) {
                    favoriteCity.setFavorite(false);
                } else {
                    favoriteCity.setFavorite(true);
                }
                cityViewModel.updateFavoriteState(favoriteCity);
                break;
            default:
               startActivity(
                       new Intent(this, MainActivity.class)
                       .putExtra(I18n.KEY_FOR_INTENT, cityList.get(position).getName())
               );
               break;
        }
    }
}
