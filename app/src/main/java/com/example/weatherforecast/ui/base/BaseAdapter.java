package com.example.weatherforecast.ui.base;

import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.ui.customclicklistener.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapter<VH extends RecyclerView.ViewHolder, T> extends RecyclerView.Adapter<VH> {
    private final List<T> items = new ArrayList<>();
    private OnItemClickListener onItemClickListener;


    public BaseAdapter(List<T> items) {
        this.items.addAll(items);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void refreshRecycler() {
        notifyDataSetChanged();
    }

    public T getItem(int position) {
        return items.get(position);
    }

    public final void setNewValues(List<T> values) {
        items.clear();
        items.addAll(values);
        refreshRecycler();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }
}
