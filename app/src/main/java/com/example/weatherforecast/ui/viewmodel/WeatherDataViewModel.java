package com.example.weatherforecast.ui.viewmodel;

import com.example.weatherforecast.data.entity.WeatherData;
import com.example.weatherforecast.data.repositories.WeatherApiRepository;

import javax.inject.Inject;

import io.reactivex.Single;

public class WeatherDataViewModel {
    private WeatherApiRepository weatherApiRepository;

    @Inject
    public WeatherDataViewModel(WeatherApiRepository weatherApiRepository) {
        this.weatherApiRepository = weatherApiRepository;
    }

    public Single<WeatherData> getWeatherByCityName(String cityName, String apiKey) {
        return weatherApiRepository.getWeatherByCityName(cityName, apiKey);
    }
}
