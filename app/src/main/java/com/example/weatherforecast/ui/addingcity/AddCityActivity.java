package com.example.weatherforecast.ui.addingcity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.App;
import com.example.weatherforecast.BuildConfig;
import com.example.weatherforecast.R;
import com.example.weatherforecast.data.entity.City;
import com.example.weatherforecast.data.entity.WeatherData;
import com.example.weatherforecast.ui.citymanagment.EditCitiesActivity;
import com.example.weatherforecast.ui.customclicklistener.OnItemClickListener;
import com.example.weatherforecast.ui.mainactivity.MainActivity;
import com.example.weatherforecast.ui.viewmodel.CityViewModel;
import com.example.weatherforecast.ui.viewmodel.WeatherDataViewModel;
import com.example.weatherforecast.utils.NetworkManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import toothpick.Toothpick;

public class AddCityActivity
        extends AppCompatActivity
        implements OnItemClickListener, TextWatcher {
    @Inject
    CityViewModel cityViewModel;
    @Inject
    WeatherDataViewModel weatherDataViewModel;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    private List<WeatherData> weatherDataList = new ArrayList<>();
    private EditText editText;
    private TextView textView;
    private WeatherDataItemAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);
        Toothpick.inject(this, App.scope());

        RecyclerView recyclerView = findViewById(R.id.cityNamesRecyclerView);
        ImageView backButton = findViewById(R.id.backBtnFromAddCityActivity);
        editText = findViewById(R.id.etCityName);
        textView = findViewById(R.id.tErrorMessage);

        setupCitiesRecyclerView(recyclerView);

        editText.addTextChangedListener(this);
        backButton.setOnClickListener(this::onClickBack);
    }

    private void setupCitiesRecyclerView(RecyclerView recyclerView) {
        adapter = new WeatherDataItemAdapter(weatherDataList);
        adapter.setOnItemClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    public void onClickBack(View v) {
        startActivity(new Intent(this, MainActivity.class));
    }

    private void onSuccess(WeatherData weatherData) {
        textView.setVisibility(View.INVISIBLE);
        weatherDataList.add(weatherData);
        adapter.setNewValues(weatherDataList);
    }

    private void onError(Throwable throwable) {
        NetworkManager.isNetworkAvailable(this);
        weatherDataList.clear();
        textView.setVisibility(View.VISIBLE);
        textView.setText(throwable.getMessage());
        throwable.printStackTrace();
    }

    @Override
    public void onItemClickListener(int position, View view) {
        cityViewModel.insertCity(new City(weatherDataList.get(position).getName(), false));
        startActivity(new Intent(this, EditCitiesActivity.class));
    }

    @Override
    public void afterTextChanged(Editable s) {
        String cityName = editText.getText().toString();
        compositeDisposable.add(
                weatherDataViewModel.getWeatherByCityName(cityName, BuildConfig.API_KEY)
                        .subscribe(this::onSuccess, this::onError)
        );
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }
}
