package com.example.weatherforecast.ui.mainactivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.R;
import com.example.weatherforecast.data.entity.City;
import com.example.weatherforecast.ui.base.BaseAdapter;
import com.example.weatherforecast.ui.customclicklistener.OnItemClickListener;

import java.util.List;

import static com.example.weatherforecast.ui.mainactivity.CitiesItemAdapter.CitiesItemViewHolder;

public class CitiesItemAdapter extends BaseAdapter<CitiesItemViewHolder, City> {

    public CitiesItemAdapter(List<City> items) {
        super(items);
    }

    @NonNull
    @Override
    public CitiesItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CitiesItemViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(
                                R.layout.item_cities,
                                parent,
                                false),
                getOnItemClickListener()
        );
    }

    @Override
    public void onBindViewHolder(@NonNull CitiesItemViewHolder holder, int position) {
        holder.bind(getItem(position).getName());
    }

    static class CitiesItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView nameCity;
        private OnItemClickListener listener;

        public CitiesItemViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;

            nameCity = itemView.findViewById(R.id.nameCity);
            nameCity.setOnClickListener(this);
        }

        public void bind(String item) {
            nameCity.setText(item);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClickListener(position, v);
            }
        }
    }
}
