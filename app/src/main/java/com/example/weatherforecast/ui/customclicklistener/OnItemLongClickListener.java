package com.example.weatherforecast.ui.customclicklistener;

import android.view.View;

public interface OnItemLongClickListener {
    void onItemLongClickListener(int position, View view);
}
